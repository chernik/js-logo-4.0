﻿var Context = this;
var CodeJSLib = {};
// --------------------------------------------------------------
var loader = function(callback){
	var Lib = new Map();
	CodeJSLib.test = function(){
		return Lib;
	}
	CodeJSLib.get = function(name){
		var func = Lib.get(name);
		if(func === undefined){
			return null;
		}
		return func;
	}
	CodeJSLib.register = function(arr){
		for(var i = 0; i < arr.length; i++){
			var func = CodeJSLib.get(arr[i].name);
			if(func != null){
				throw "Function already exist at " + i;
			}
			Lib.set(arr[i].name, arr[i]);
		}
	}
	CodeJSLib.registerFile = function(file, cb){
		var obj = {};
		obj.Context = Context;
		obj.format = Context.format;
		importFile(file, obj, function(){
			var err;
			try{
				CodeJSLib.register(obj.data);
				cb();
			}catch(err){
				cb(err);
			}
		});
	}
	importFile("/format.js", Context, callback);
}
// -------------------------------------------------------------
loader(function(){
	Context.CodeJSLib = CodeJSLib;
	callback();
});