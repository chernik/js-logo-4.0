﻿var Context = this;
var CodeLib = {};
// ---------------------------------------------------------------
var glogers = [
	{
		name: "(",
		type: "glog"
	}, {
		name: ")",
		type: "glog"
	}
]
// --------------------------------------------------------------
var loader = function(callback){
	var Lib = new Map();
	CodeLib.test = function(){
		return Lib;
	}
	CodeLib.get = function(name){
		var func = Lib.get(name);
		if(func === undefined){
			return null;
		}
		return func;
	}
	CodeLib.register = function(arr){
		for(var i = 0; i < arr.length; i++){
			var func = CodeLib.get(arr[i].name);
			if(func != null){
				throw "Function already exist at " + i;
			}
			Lib.set(arr[i].name, arr[i]);
		}
	}
	CodeLib.registerFile = function(file, cb){
		var obj = {};
		importFile(file, obj, function(){
			CodeLib.register(obj.data);
			cb();
		});
	}
	callback();
}
// -------------------------------------------------------------
loader(function(){
	Context.CodeLib = CodeLib;
	CodeLib.register(glogers);
	callback();
});