﻿var format = {
	norma: function(val){
		if(typeof(val) != "object"){
			return {
				type: typeof(val),
				data: val
			};
		}
		if(val.type === undefined){
			return {
				type: "array",
				data: val
			};
		}
		return val;
	},
	get: function(val, type){
		val = format.norma(val);
		if(type === null){
			return val.data;
		}
		var rtype = type;
		if(rtype == "boolean"){
			rtype = "string";
		}
		if(val.type == "string" && rtype == "number"){
			rtype = "string";
		}
	
		if(val.type != rtype){
			throw "Wrong type '" + type + "'";
		}
		var data = val.data;
		if(type == "boolean"){
			data = data == "true" || data == "да";
		}
		if(type == "number"){
			data = +data;
			if(data != data){
				throw "Wrong type '" + type  + "'";
			}
		}
		return data;
	},
	put: function(val){
		if(typeof(val) == "boolean"){
			return {
				type: "string",
				data: val ? "true" : "false"
			}
		}
		if(typeof(val) == "number"){
			return {
				type: "number",
				data: val
			}
		}
		if(typeof(val) == "object"){
			if(val.type === undefined){
				return {
					type: "array",
					data: val
				}
			} else {
				return val;
			}
		}
		if(typeof(val) == "string"){
			return {
				type: "string",
				data: val
			}
		}
		throw "Wrong output type";
	}
};

this.format = format;

callback();