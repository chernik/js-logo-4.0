﻿function importFile(file, space, cb){
	importText(file, function(txt){
		var f = new Function("importFile, importText, callback", txt);
		f.call(space, importFile, importText, cb);
	});
}

function importText(file, cb){
	var req = new XMLHttpRequest();
	req.onload = function(e){
		if(req.status != 200){
			throw "Server response > " + req.status;
		}
		var txt = req.responseText;
		cb(txt);
	}
	req.onerror = function(e){
		console.log("Import error > " + e);
	}
	req.open("GET", file);
	req.send();
}