﻿var Context = this;
var W = self;

// ------------------------------------------------------------------------------------

function importFile(file, space, cb){
	importText(file, function(txt){
		var f = new Function("importFile, importText, callback", txt);
		f.call(space, importFile, importText, cb);
	});
}

function importText(file, cb){
	var req = new XMLHttpRequest();
	req.onload = function(e){
		if(req.status != 200){
			throw "Server response > " + req.status;
		}
		var txt = req.responseText;
		cb(txt);
	}
	req.onerror = function(e){
		console.log("Import error > " + file);
		console.log(e);
	}
	req.open("GET", file);
	req.send();
}

// ------------------------------------------------------------------------------------

Context.threads = [];
Context.threadpos = 0;
Context.islog = false;
Context.vars = {};
Context.waitths = [];

// ------------------------------------------------------------------------------------

Context.error = [];
Context.setError = function(err, com, th){
	console.error(err);
	Context.error = [err.toString(), com, th];
	W.postMessage({
		type: "error",
		data: Context.error
	});
}

// -----------------------------------------------------------------------------------

Context.step = function(){
	for(var iter = 0; iter < 1000; iter++){
		if(Context.threads.length == 0){
			break;
		}
		if(Context.threadpos >= Context.threads.length){
			Context.threadpos = 0;
		}
		var th = Context.threads[Context.threadpos];
		Context.threadpos++;
		if(th.compos >= th.coms.length){
			Context.threads = Context.threads.
				slice(0, Context.threadpos-1).
					concat(Context.threads.slice(Context.threadpos));
			Context.threadpos--;
			continue;
		}
		var com = th.coms[th.compos];
		th.compos++;
		if(com.type == "data"){
			if(Context.islog){
				console.log("Data [" + th.id.data + "] > " + com.data);
			}
			th.stackData.push({
				type: com.kind,
				data: com.data
			});
			continue;
		}
		if(com.type == "func"){
			if(Context.islog){
				console.log("Func [" + th.id.data + "] > " + com.func.name);
				console.log(com);
			}
			var func = com.func;
			var kerFunc = Context.kerFunc[func.name];
			if(kerFunc !== undefined){
				var err;
				var isRet;
				try{
					isRet = kerFunc(th);
					if(isRet === undefined){
						isRet = false;
					}
					if(isRet != com.isReturn){
						throw "Wrong return";
					}
				} catch(err) {
					err = err + "\nAt system call '" + func.name + "'";
					Context.setError(err, com, th);
					th.compos = th.coms.length;
				}
				continue;
			}
			//var logoFunc = Context.CodeLib.get(func.name);
			var logoFunc = func;
			th.multi = com.multi;
			th.lastFunc = logoFunc;
			if(logoFunc === undefined){
				Context.setError("Unknown func > " + func.name, com, th);
				th.compos = th.coms.length;
				continue;
			}
			if(logoFunc.codearr === undefined){
				var err;
				try {
					logoFunc.codearr = Context.LogoParse.text2array(
						logoFunc.code.
							split("\n").join(" ").
							split("\r").join(" ").
							split("	").join(" ")
					);
				} catch(err) {
					err = err + "\nAt parsing code of '" + logoFunc.name + "'";
					Context.setError(err, com, th);
					th.compos = th.coms.length;
					continue;
				}
			}
			if(logoFunc.runnable === undefined){
				var err;
				try {
					logoFunc.runnable = Context.LogoParse.array2code(
						logoFunc.codearr
					);
				} catch(err) {
					err = err + "\nAt compile '" + logoFunc.name + "'";
					Context.setError(err, com, th);
					th.compos = th.coms.length;
					continue;
				}
			}
			th.stackRet.push(com.isReturn);
			th.coms = th.coms.slice(0, th.compos).concat(
				logoFunc.runnable).concat(
				th.coms.slice(th.compos));
			th.compos;
			continue;
		}
		throw "Wrong type";
	}
	setTimeout(Context.step, 10);
}
// ------------------------------------------------------------------------------------

Context.listeners = {
	"addThread": function(args){
		var e = args.code;
		var id = args.id;
		var err;
		var runnable;
		try {
			runnable = Context.LogoParse.array2code(
				Context.LogoParse.text2array(
					e.
						split("\n").join(" ").
						split("\r").join(" ").
						split("	").join(" ")
			));
		} catch (err){
			err = err + "\nAt message 'addThread'";
			Context.setError(err, null, null);
			return;
		}
		Context.threads.push({
			coms: runnable, // Команды
			compos: 0, // Подиция текущей команды
			stackData: [], // Данные
			stackFunc: [{vars:{}}], // Локальные переменные
			stackRet: [], // Нужно ли возвращаемое значение
			multi: undefined, // Была ли последняя функция мультифункцией
			lastFunc: undefined, // Последняя не ядровая функция
			loops: [], // Позиции петель
			id: id === undefined ? e : id
		});
	},
	"start": function(e){
		Context.step();
	},
	"registerEval": function(e){
		Context.CodeJSLib.registerFile(e, function(err){
			if(err === undefined){
				W.postMessage({
					type: "log",
					data: "Loaded!"
				});
			} else {
				W.postMessage({
					type: "error",
					data: err.toString()
				});
			}
		});
	},
	"registerLogoFile": function(e){
		importText(e, function(txt){
			CodeLib.register(
				LogoParse.lib2funcs(txt)
			);
		});
	},
	"setLog": function(e){
		Context.islog = e;
	},
	"stopAll": function(e){
		Context.kerFunc["stopall"]();
	},
	"eval": function(e){
		var th = Context.waitths[e.th];
		Context.waitths[e.th] = undefined;
		if((e.res === undefined) !== !th.isReturn){
			throw "Wrong return at eval function " + th.name;
		}
		if(e.res !== undefined){
			th.th.stackData.push(e.res);
		}
		th.th.compos++;
	}
}

// --------------------------------------------------------------------------------------

W.onmessage = function(e){
	var f = Context.listeners[e.data.name];
	if(f === undefined){
		Context.setError("Wrong command", null, null);
		return;
	}
	f(e.data.args);
}

// -------------------------------------------------------------------------------------

importFile("/logoparse.js", Context, function(){ // Парсер языка и ЛОГО библиотека
	Context.LogoParse.file2funcs("/kernellib.txt", function(funcs){ // Базовая библиотека
		Context.CodeLib.register(funcs);
		importFile("/kernelcom.js", Context, function(){ // Системные команды
			importFile("/codejslib.js", Context, function(){ // JS библиотека
				CodeJSLib.registerFile("/kerneleval.js", function(){ // Для работы базовой библиотеки
					W.postMessage({
						type: "log",
						data: "Loaded!"
					});
				});
			});
		});
	});
});