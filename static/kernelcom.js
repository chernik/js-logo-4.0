﻿var Context = this;
var islog = [];

function tobool(str){
	return Context.format.get(str, "boolean");
}

function getVar(th, name, fl){
	if(name === undefined || name.type == "array"){
		throw "Cannot get name (maybe empty stack or name is array)";
	}
	var val;
	for(
		var i = th.stackFunc.length - 1;
		i >= 0 && (val = th.stackFunc[th.stackFunc.length-1].vars[name.data]) === undefined; i--
	);
	if(val === undefined && fl === undefined){
		val = Context.vars[name.data];
	}
	return val;
}

Context.kerFunc = {
	"__get": function(th){
		var delta = th.stackData.pop();
		if(delta === undefined || delta.type != "number"){
			throw "Wrong delta (empty stack or nonumber value) in '__get'";
		}
		delta = th.stackData.length - 1 - delta.data;
		var val = th.stackData[delta];
		if(val === undefined){
			throw "Wrong delta (wrong pos in stack) in '__get'";
		}
		th.stackData = th.stackData.slice(0, delta).concat(
			th.stackData.slice(delta+1)).concat([val]);
		return true;
	},
	"stop": function(th){
		var i;
		for(i = th.compos; i < th.coms.length && (th.coms[i].type != "func" || th.coms[i].func.name != "__end"); i++);
		th.compos = i;
	},
	"op": function(th){
		if(th.stackRet[th.stackRet.length-1] == false){
			throw "Function cannot return value in 'op'";
		}
		th.stackRet[th.stackRet.length-1] = false;
		Context.kerFunc["stop"](th);
	},
	"__getmulti": function(th){
		//var val = th.coms[th.compos-1].multi;
		var val = th.multi;
		if(val === undefined){
			val = -1;
		}
		th.stackData.push({
			type: "number",
			data: val
		});
	},
	"__let": function(th){
		var val = th.stackData.pop();
		if(val === undefined){
			throw "Cannot get value (maybe empty stack)";
		}
		var name = th.stackData.pop();
		if(name === undefined || name.type == "array"){
			throw "Cannot set name (maybe empty stack or name is array)";
		}
		th.stackFunc[th.stackFunc.length-1].vars[name.data] = val;
	},
	"thing": function(th){
		var name = th.stackData.pop();
		val = getVar(th, name);
		if(val === undefined){
			debugger;
			throw "Cannot get value (cannot find in vars '" + name.data + "')";
		}
		th.stackData.push(val);
		return true;
	},
	"name?": function(th){
		var name = th.stackData.pop();
		var val = getVar(th, name);
		if(val === undefined){
			val = "false";
		} else {
			val = "true";
		}
		th.stackData.push({
			type: "string",
			data: val
		});
		return true;
	},
	"localname?": function(th){
		var name = th.stackData.pop();
		var val = getVar(th, name, 1);
		if(val === undefined){
			val = "false";
		} else {
			val = "true";
		}
		th.stackData.push({
			type: "string",
			data: val
		});
		return true;
	},
	"__begin": function(th){
		th.stackFunc.push({
			vars: {},
			func: th.lastFunc
		});
	},
	"__checkret": function(th){
		if(th.stackRet.pop() == true){
			throw "Function must return value";
		}
	},
	"__end": function(th){
		th.stackFunc.pop();
	},
	"if": function(th){
		var runnable = th.stackData.pop();
		if(runnable === undefined || runnable.type != "array"){
			throw "Cannot get runnable (maybe empty stack or runnable is noarray) in 'if'";
		}
		var cond = th.stackData.pop();
		if(cond === undefined){
			throw "Cannot get cond (maybe empty stack) in 'if'";
		}
		cond = cond.type == "string" && tobool(cond.data);
		runnable = Context.LogoParse.array2code(runnable.data);
		if(cond){
			th.coms = th.coms.slice(0, th.compos).
				concat(runnable).concat(
				th.coms.slice(th.compos));
			th.compos;
		}
	},
	"__concat": function(th){
		var array2 = th.stackData.pop();
		if(array2 === undefined){
			throw "Cannot get arrays (maybe empty stack) in '__concat'";
		}
		if(array2.type != "array"){
			array2 = [array2];
		} else {
			array2 = array2.data;
		}
		var array1 = th.stackData.pop();
		if(array1 === undefined){
			throw "Cannot get arrays (maybe empty stack) in '__concat'";
		}
		if(array1.type != "array"){
			array1 = [array1];
		} else {
			array1 = array1.data;
		}
		th.stackData.push({
			type: "array",
			data: array1.concat(array2)
		});
		return true;
	},
	"__eval": function(th){
		var args = th.stackData.pop();
		if(args === undefined){
			throw "Cannot get arguments (empty stack)";
		}
		var name = th.stackData.pop();
		if(name	=== undefined || name.type == "array"){
			throw "Cannot get name (empty stack or name is array)";
		}
		var f = Context.CodeJSLib.get(name.data);
		if(f == null){
			var isReturn = th.coms[th.compos-1].isReturn;
			th.coms = th.coms.slice(0, th.compos).
				concat({
					isReturn: false,
					type: "func",
					func: Context.CodeLib.get("__pause")
				}).concat(th.coms.slice(th.compos));
			var i;
			for(i = 0; Context.waitths[i] !== undefined; i++);
			Context.waitths[i] = {
				name: name.data,
				isReturn: isReturn,
				th: th
			};
			W.postMessage({
				type: "eval",
				data: {
					name: name.data,
					args: args,
					th: i
				}
			});
			return isReturn;
			// throw "Wrong func name: '" + name.data + "'";
		}
		var err, res;
		try {
			res = f.runnable(args);
		} catch(err) {
			throw err + "\nAt eval function '" + name.data + "'";
		}
		if(res === undefined){
			return false;
		}
		th.stackData.push(res);
		return true;
	},
	"__log": function(th){
		console.log(JSON.parse(JSON.stringify(th)));
	},
	"__debugger": function(th){
		debugger;
	},
	"__fork": function(th){
		var id = th.stackData.pop();
		if(id == undefined){
			throw "Cannot get id (maybe empty stack) in '__fork'";
		}
		var newTh = {
			coms: th.coms.slice(0), // Команды не копируются
			compos: th.compos, // Позиция опируется
			stackData: th.stackData.slice(0), // Стек ядра копируется, т. к. данные в нем не изменяются
			stackFunc: JSON.parse(JSON.stringify(th.stackFunc)), // Локальные переменные копируются полностью
			stackRet: th.stackRet.slice(0),
			multi: th.multi,
			lastFunc: th.lastFunc,
			loops: th.loops.slice(0),
			id: id
		};
		th.stackData.push({
			type: "string",
			data: "false"
		});
		newTh.stackData.push({
			type: "string",
			data: "true"
		});
		Context.threads.push(newTh);
		return true;
	},
	"__loop": function(th){
		th.loops.push(th.compos-1);
	},
	"__backloop": function(th){
		var loopclear = th.loops.pop();
		var loopstart = th.loops.pop();
		th.coms =
			th.coms.slice(0, loopclear).concat(
			th.coms.slice(th.compos)
		);
		th.compos = loopstart;
	},
	"__clearloop": function(th){
		th.loops.pop();
		th.loops.pop();
	},
	"__put": function(th){ // )))))))))))))))
	},
	"__echolog": function(th){
		var state = th.stackData.pop();
		if(state == undefined || state.type != "string"){
			throw "Wrong state (maybe empty stack or state is nostring)";
		}
		state = tobool(state.data);
		if(state){
			if(islog.length == 0){
				return;
			}
			Context.islog = islog.pop();
		} else {
			islog.push(Context.islog);
			Context.islog = false;
		}
	},
	"stopme": function(th){
		th.compos = th.coms.length;
	},
	"stopall": function(th){
		console.log("Stop " + Context.threads.length + " threads");
		Context.threads = [];
	},
	"make": function(th){
		var data = th.stackData.pop();
		var name = th.stackData.pop();
		if(name === undefined){
			throw "Empty stack for name var";
		}
		if(name.type == "array"){
			throw "Wrong type for name: array";
		}
		if(data === undefined){
			throw "Empty stack for data";
		}
		name = name.data;
		Context.vars[name] = data;
	},
	"__pause": function(th){
		th.compos--;
	}
}

callback();