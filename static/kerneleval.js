﻿var Context = this;

var get = Context.format.get;
var put = Context.format.put;
var norma = Context.format.norma;

Context.data = [
	{
		name: "compare", // Сравнение объектов
		runnable: function(args){
			args = get(args, "array");
			var f = get(args[0], "string");
			var a = get(args[1], null);
			var b = get(args[2], null);
			switch(f){
				case "<": return put(a < b);
				case ">": return put(a > b);
				case "==": return put(a == b);
				case "===": return put(a === b);
				default: throw "Wrong comand '" + f + "'";
			}
		}
	}, {
		name: "arrays_concatrep", // [1,2,3] => [1,2,3,1,2,3,1,2,3]
		runnable: function(args){
			args = get(args, "array");
			var count = get(args[0], "number");
			var elm = get(args[1], "array");
			var arr = [];
			for(var i = 0; i < count; i++){
				arr = arr.concat(elm);
			}
			return put(arr);
		}
	}, {
		name: "boolean_op", // Логические операции
		runnable: function(args){
			args = get(args, "array");
			var f = get(args[0], "string");
			switch(f){
			case "!":
				return put(
					!get(args[1], "boolean")
				);
			case "&&":
				return put(
					get(args[1], "boolean") && get(args[2], "boolean")
				);
			case "||":
				return put(
					get(args[1], "boolean") || get(args[2], "boolean")
				);
			default:
				throw "Wrong operation";
			}
		}
	}, {
		name: "arrays_raml", // a => [a]
		runnable: function(args){
			return put([args]);
		}
	}, {
		name: "log", // Вывод в JS сонсоль
		runnable: function(args){
			function post(str){
				postMessage({
					type: "log",
					data: str
				});
			}
			if(args.type == "array"){
				console.log(args);
				post("[" + args.data.join(" ") + "]");
			} else {
				post(args.data);
			}
		}
	}, {
		name: "numbers_op", // Операции над числами
		runnable: function(args){
			args = get(args, "array");
			var f = get(args[0], "string");
			switch(f){
			case "+":
				return put(
					get(args[1], "number") + get(args[2], "number")
				);
			case "-":
				return put(
					get(args[1], "number") - get(args[2], "number")
				);
			case "*":
				return put(
					get(args[1], "number") * get(args[2], "number")
				);
			case "/":
				return put(
					get(args[1], "number") / get(args[2], "number")
				);
			}
		}
	}, {
		name: "arrays_item", // Элемент массива (еще для строк надо сделать)
		runnable: function(args){
			args = get(args, "array");
			var pos = get(args[0], "number");
			var arr = get(args[1], "array");
			if(pos <= 0 || pos > arr.length){
				throw "Wrong index '" + pos + "' in array '" + arr + "'";
			}
			return put(arr[pos - 1]);
		}
	}, {
		name: "arrays_count", // Количество элементов в массиве (еще для строк надо сделать)
		runnable: function(args){
			var arr = get(args, "array");
			return put(arr.length);
		}
	}
];

callback();