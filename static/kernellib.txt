﻿'Запуск функции JS, подробнее см. CodeJSLib
to "__eval :name :args

-----------------------------------------------------------------------------------------------
Отладка

'Вывод текущего состояния потока в консоль JS
to "__log

'Приостановка выполнения в среде JS
to "__debugger

'true - вернуть логгинг, false - выключить логгинг
to "__echolog :state

-----------------------------------------------------------------------------------------------
Прочее

'Конкатекация массивов
to "__concat :arr1 :arr2

'Проверить на возврат значения
to "__checkret

'Удаляет процесс
to "stopme

'Удаляет все процессы
to "stopall

to "__pause

-----------------------------------------------------------------------------------------------
Команды работы со стеком ядра

'Возвращает аргумент, бывший на :delta месте относительно вершины стека ядра
to "__get :delta

'Кладет :data на вершину стека ядра
to "__put :data

'Кладет :val на стек ядра и далее работает как stop
to "op :val

'Кладет в стек ядра количество входных параметров для multifunc (-1, если multifunc запущена без скобок, или это не multifunc)
to "__getmulti

----------------------------------------------------------------------------------------------
Работа с кадровым стеком

'Проматывает до __end и исполняет, начиная с нее
to "stop

'Создать переменную в кадре стека
to "__let :name :value

'Взять значение переменной
to "thing :name

'Сказать, существует ли переменная
to "name? :name

'Сказать, существует ли локальная переменная
to "localname? :name

'Создать новый кадр стека
to "__begin

'Удалить кадр стека
to "__end

'Создать глобальную переменную
to "make :name :data

-----------------------------------------------------------------------------------------------
Выполнение массивов

'Условный оператор
to "if :cond :runnable

'Запуск нового потока с именем :id (по нему производится доступ к потоку). Возващает строку, причем "true в порождающем и "false порожденном потоках
to "__fork :id

-----------------------------------------------------------------------------------------------
Петли

'Старт петли
to "__loop

'Стирает все команды назад до первого __loop и переводит управление на второй __loop
to "__backloop

'Стирает метку __loop
to "__clearloop

------------------------------------------------------------------------------------------------
Команды для заголовка функций

'Оператор исполнения кода в текущем потоке
to `run :runnable
__echolog "false
__let "__system_if __get 1
__let "__system_if __concat __concat [] :__system_if []
__echolog "true
if "true :__system_if
__checkret
end

'Оператор повтора
to `repeat :count :runnable
__echolog "false
__let "__system_runnable __get 1
__let "__system_count    __get 1
__let "__system_runnable __eval "arrays_concatrep __concat
	:__system_count
	__eval "arrays_raml :__system_runnable
__let "__system_runnable __concat __concat [] :__system_runnable []
__echolog "true
run :__system_runnable
__checkret
end

'Меньше?
to `less? :a :b
__echolog "false
__begin
__let "__system2 __get 1
__let "__system1 __get 1
op __eval "compare __concat __concat "< :__system1 :__system2
__end
__checkret
__echolog "true
end


'Отрицание
to `not :a
__echolog "false
__begin
op __eval "boolean_op __concat "! __get 2
__end
__checkret
__echolog "true
end

'Расширенный условный оператор
to `ifelse :cond :iftrue :iffalse
__echolog "false
__let "__system_iffalse __get 1
__let "__system_iftrue  __get 1
__let "__system_cond    __get 1
__let "__system_iftrue __concat __concat [] :__system_iftrue [ __let "__system_cond "true]
__let "__system_iffalse __concat __concat [] :__system_iffalse []
__echolog "true
if     :__system_cond :__system_iftrue
if not :__system_cond :__system_iffalse
__checkret
end

-----------------------------------------------------------------------------------------------
Работа с массивами

'Возвращает список своих параметров (list "a [b c] => [a [b c]])
to list :a :b ... :arr
__echolog "false
if localname? "a [
	__let "arr __concat
		__eval "arrays_raml :a
		__eval "arrays_raml :b
	__echolog "true
	op :arr
]
__echolog "true
op :arr
end

'Возвращает элемент списка (нумерация с 1)
to item :ind :arr
__echolog "false
__let "val __eval "arrays_item list :ind :arr
__echolog "true
op :val
end

'Возвращает длину массива
to count :arr
__echolog "false
__let "val __eval "arrays_count :arr
__echolog "true
op :val
end

'Возвращает конкатенацию своих аргументов (se "a [b c] => [a b c])
to se :a :b ... :arr
__echolog "false
if localname? "a [
	__echolog "true
	op __concat :a :b
]
__let "res []
dotimes list "i count :arr [
	__let "res __concat :res item sum :i 1 :arr
]
__echolog "true
op :res
end

-----------------------------------------------------------------------------------------------
Команды launch, forever & waituntil и функции, необходимые для их работы

'Сумма чисел
to sum :a :b
__echolog "false
__let "res __eval "numbers_op ( list "+ :a :b )
__echolog "true
op :res
end

'(:name = [<name> <count>]) Выполняет :runnable <count> раз, занося в :<name> число от 0 до <count>-1 для каждой итерации
to `dotimes :name :runnable
__echolog "false
__let "__system_runnable __get 1
__let "__system_name __get 1
__let "__system_val item 1 :__system_name
__let :__system_val 0
__let "__system_runnable __concat :__system_runnable [
	__let "__system_val __get 1
	__let :__system_val sum thing :__system_val 1
	__put :__system_val
	
]
__put :__system_val
__let "__system_name item 2 :__system_name
__echolog "true
repeat :__system_name :__system_runnable
__echolog "false
__let "__system_val __get 1
__checkret
__echolog "true
end

'Запуск :runnable в отдельном потоке
to `launch :runnable
__let "__system_laurun __get 1
if __fork :__system_laurun [
	run :__system_laurun
	stopme
]
__checkret
end

'Запуск бесконечно выполняющегося :runnable в отдельном потоке
to `forever :runnable
__let "__system_forrun __get 1
if __fork :__system_forrun [
	__let "__system_forrun ( se [__loop] :__system_forrun [__backloop] )
	__put :__system_forrun
	__loop
	__let "__system_forrun __get 1
	__put :__system_forrun
	if "true :__system_forrun
]
__checkret
end

to `waituntil :cond
__let "__system_cond __get 1
__let "__system_cond ( se
	[__loop ifelse]
	:__system_cond
	[[__clearloop] [__backloop]] )
__put :__system_cond
__loop
__let "__system_cond __get 1
__put :__system_cond
if "true :__system_cond
__let "__system_cond __get 1
__checkret
end

--------------------------------------------------------------------------------------------------------------
Операторы

to 10 + :a :b
op __eval "numbers_op ( list "+ :a :b )
end

to 10 - :a :b
op __eval "numbers_op ( list "- :a :b )
end

to 11 * :a :b
op __eval "numbers_op ( list "* :a :b )
end

to 11 / :a :b
op __eval "numbers_op ( list "/ :a :b )
end

to 9 < :a :b
op __eval "compare ( list "< :a :b )
end

to 9 > :a :b
op __eval "compare ( list "> :a :b )
end

to and :a :b ... :arr
if localname? "a [
	op __eval "boolean_op ( list "&& :a :b )
]
__let "res "true
dotimes list "i count :arr [
	__let "res and :res item sum :i 1 :arr
	if not :res [
		op :res
	]
]
op :res
end

'Потом переделаю
to 8 = :a :b
op and ( not :a < :b ) (not :a > :b)
end
