﻿var LogoParse = {};
var CodeLib = {};
var TextParse = {};
var Context = this;
// --------------------------------------------------------------------------------------------------------------
var loader = function(callback){
	LogoParse.text2array = function(str){
		var reg = ">0 m sp 2 g 1 '\" 34 p '| 10 m '| 4 >1 c g 1 p g 12 sp 4 >1 c g 1 '[ 4 >1 c g 46 '] 4 >1 c g 51 p g 21 '| 2 g 11 '[ 3 >2 g 1 '] 1 stop g 27";
		var fs = [
			function(str, pind, buf, arr){
				arr.push([]);
			},
			function(str, pind, buf, arr){
				arr[arr.length - 1].push(buf);
			},
			function(_str, pind, buf, arr){
				TextParse.reg(str, reg, fs, pind, arr);
				var res = arr.pop();
				arr[arr.length - 1].push(res);
			}
		];
		var res, err;
		try{
			res = TextParse.reg(str, reg, fs)[0];
		}catch(err){
			var pos = +err.split("at ")[1];
			if(pos != pos){
				throw err;
			}
			throw "Cannot parse array\n" + err + "\n" + str.substring(0, pos) + String.fromCharCode(190) + (str[pos] === undefined ? "<end>" : str[pos]);
		}
		return res;
	}
	LogoParse.array2code = function(arr){
		var i;
		function getType(el){
			if(typeof(el[0]) == "object"){
				return "array";
			}
			if(typeof(el[0]) == "string"){
				if(el[0][0] == '"'){
					el[0] = el[0].substring(1);
					return "string";
				}
				if(el[0][0] == ":"){
					var arr = [];
					arr.push({
						type: "data",
						kind: "string",
						data: el[0].substring(1)
					});
					var f = CodeLib.get("thing");
					if(f == null){
						throw "Cannot use :<var>, cannot find 'thing' function, at " + i;
					}
					arr.push({
						isReturn: true,
						type: "func",
						argc: 1,
						func: f
					});
					el[0] = arr;
					return "variable";
				}
				var num = +el[0];
				if(num == num){
					el[0] = num;
					return "number";
				}
				var f = CodeLib.get(el[0]);
				if(f == null){
					throw "Wrong func " + el[0] + " at " + i;
				}
				el[0] = f;
				return "func";
			}
			throw "Unknown type " + el[0] + " at " + i;
		}
		var stackData = [];
		var stackFunc = [];
		var f;
		var sk = false;
		for(i = 0; i < arr.length; i++){
			var el = [arr[i]];
			var type = getType(el);
			if(type == "func"){
				if(el[0].type == "glog"){
					if(el[0].name == "("){
						if(stackFunc.length == 0){
							throw "( return nothing at " + i;
						}
						f = stackFunc.pop();
						f.argc--;
						if(f.argc < 0){
							throw "Excess () at " + i;
						}
						stackFunc.push(f);
						stackFunc.push({
							argc: 1,
							func: el[0]
						});
						sk = true;
						continue;
					}
					sk = false;
					while(true){
						if(stackFunc.length == 0){
							throw "Cannot find ( at " + i;
						}
						f = stackFunc.pop();
						if(f.func.type == "glog"){
							break;
						}
						if(f.argc > 0 && !f.isMulti){
							throw "Cannot get data at " + i;
						}
						var forData = {
							type: "func",
							isReturn: f.isReturn,
							func: f.func
						};
						if(f.isMulti){
							forData.multi = -f.argc;
						}
						stackData.push(forData);
					}
				}
				if(el[0].type == "operator"){
					sk = false;
					while(true){
						if(stackFunc.length == 0){
							throw "Cannot get data at " + i;
						}
						f = stackFunc.pop();
						if(f.func.type == "glog"){
							if(f.argc != 0){
								throw "Cannot get data at " + i;
							}
							stackFunc.push(f);
							stackFunc.push({
								isReturn: true,
								argc: 1,
								func: el[0]
							});
							break;
						}
						if(f.func.type == "operator"){
							if(f.argc > 0){
								throw "Excess data at " + i;
							}
							if(f.func.order >= el[0].order){
								stackData.push({
									type: "func",
									isReturn: f.isReturn,
									func: f.func
								});
								if(stackFunc.length > 0){
									//stackFunc[stackFunc.length-1].argc--;
								}
								continue;
							}
						}
						if(f.func.inp == 0){
							if(!f.isReturn){
								throw "Cannot get data at " + i;
							}
							stackFunc.push(f);
							stackFunc.push({
								isReturn: true,
								argc: 1,
								func: el[0]
							});
							break;
						} else {
							//f.argc++;
							if(f.argc > f.func.inp){
								throw "Cannot get data at " + i;
							}
							stackFunc.push(f);
							stackFunc.push({
								isReturn: true,
								argc: 1,
								func: el[0]
							});
							break;
						}
					}
				}
				if(el[0].type == "function" || el[0].type == "multifunc"){
					var glogers = [];
					while(true){
						if(stackFunc.length == 0){
							while(glogers.length > 0){
								stackFunc.push(glogers.pop());
							}
							stackFunc.push({
								isReturn: false,
								argc: el[0].type == "multifunc" && sk ? 0 : el[0].argc,
								func: el[0],
								isMulti: sk && el[0].type ==  "multifunc"
							});
							sk = false;
							break;
						}
						f = stackFunc.pop();
						if(f.func.type == "glog"){
							if(f.argc != 1){
								throw "Excess data at " + i;
							}
							f.argc--;
							stackFunc.push(f);
							stackFunc.push({
								isReturn: true,
								argc: el[0].type == "multifunc" && sk ? 0 : el[0].argc,
								func: el[0],
								isMulti: sk && el[0].type == "multifunc"
							});
							sk = false;
							break;
							glogers.push(f);
							continue;
						}
						if(f.argc == 0 && !f.isMulti){
							stackData.push({
								isReturn: f.isReturn,
								type: "func",
								func: f.func
							});
							/*if(stackFunc.length > 0){
								stackFunc[stackFunc.length-1].argc--;
							}*/
							continue;
						}
						f.argc--;
						stackFunc.push(f);
						while(glogers.length > 0){
							stackFunc.push(glogers.pop());
						}
						stackFunc.push({
							isReturn: true,
							argc: el[0].type == "multifunc" && sk ? 0 : el[0].argc,
							func: el[0],
							isMulti: sk && el[0].type == "multifunc"
						});
						sk = false;
						break;
					}
				}
			} else {
				sk = false;
				while(true){
					if(stackFunc.length == 0){
						throw "Excess data at " + i;
					}
					f = stackFunc.pop();
					f.argc--;
					if(f.argc < 0 && !f.isMulti){
						stackData.push({
							type: "func",
							isReturn: f.isReturn,
							func: f.func
						});
						continue;
					}
					stackFunc.push(f);
					if(type == "variable"){
						stackData.push(el[0][0]);
						stackData.push(el[0][1]);
					} else {
						stackData.push({
							type: "data",
							kind: type,
							data: el[0]
						});
					}
					break;
				}
			}
		}
		while(true){
			if(stackFunc.length == 0){
				break;
			}
			f = stackFunc.pop();
			if(f.argc > 0){
				throw "Cannot get data at " + i;
			}
			stackData.push({
				type: "func",
				isReturn: f.isReturn,
				multi: f.isMulti ? -f.argc : undefined,
				func: f.func
			});
			/*if(stackFunc.length > 0 && f.func.type == "operator"){
				stackFunc[stackFunc.length-1].argc--;
			}*/
		}
		return stackData;
	}
	LogoParse.lib2funcs = function(txt){
		txt = txt.split("\r\n").join("\n");
		var arr = txt.split("\n");
		var funcs = [];
		for(var i = 0; i < arr.length; i++){
			arr[i] = TextParse.normalizeSpaces(arr[i]);
			var spl = arr[i].split(" ");
			if(spl.length < 2){
				continue;
			}
			if(spl[0] != "это" && spl[0] != "to"){
				continue;
			}
			var f = {};
			if(i > 0 && arr[i-1][0] == "'"){
				f.help = arr[i-1].substring(1) + "\n" + arr[i];
			}
			var argc = [];
			var num = +spl[1];
			var code = "";
			var nocode = false;
			var noheader = false;
			if(num == num){
				if(spl.length < 5){
					throw "Cannot collect operator at line " + i;
				}
				if(spl[2][0] == ":" || spl[3][0] != ":" || spl[4][0] != ":"){
					throw "Wrong vars at line " + i;
				}
				f.order = num;
				f.type = "operator";
				f.name = spl[2];
				argc.push(spl[3].substring(1));
				argc.push(spl[4].substring(1));
			} else {
				if(spl[1][0] == "'"){
					if(spl.length < 3){
						throw "Wront link at line " + i;
					}
					f.link = spl[2];
					f.name = spl[1].substring(1);
					funcs.push(f);
					continue;
				}
				if(spl[1][0] == '"'){
					spl[1] = spl[1].substring(1);
					nocode = true;
				}
				if(spl[1][0] == '`'){
					spl[1] = spl[1].substring(1);
					noheader = true;
				}
				if(spl[1][0] == ":"){
					throw "Wrong vars at line " + i;
				}
				f.type = "function";
				f.name = spl[1];
				for(var j = 2; j < spl.length; j++){
					if(spl[j][0] == ":"){
						argc.push(spl[j].substring(1));
					} else {
						if(spl[j] != "..."){
							throw "Cannot collect multifunc at line " + i;
						}
						if(spl.length <= j + 1){
							throw "Cannot collect multifunc at line " + i;
						}
						if(spl[j+1][0] != ":"){
							throw "Cannot collect multifunc at line " + i;
						}
						argc.push(spl[j+1].substring(1));
						f.type = "multifunc";
						break;
					}
				}
			}
			if(f.type == "multifunc"){
				f.argc = argc.length-1;
			} else {
				f.argc = argc.length;
			}
			if(!nocode){
				code = "";
				for(var j = i+1; j < arr.length; j++){
					code += arr[j] + "\n";
					arr[j] = TextParse.normalizeSpaces(arr[j]);
					spl = arr[j].split(" ");
					if(spl.length < 1){
						continue;
					}
					if(spl[0] != "конец" && spl[0] != "end"){
						continue;
					}
					break;
				}
				spl = code.split("\n");
				spl.pop(); spl.pop();
				code = spl.join("\n");
				var header = "";
				// ----------------------------------- Заполняем header
				header += '__echolog "false\n';
				if(f.type == "multifunc"){
					header += '__getmulti\n';
				}
				header += '__begin\n';
				if(f.type == "multifunc"){
					header += '__let "__system_multi __get 1\n';
					header += 'ifelse less? :__system_multi 0 [\n';
				}
				for(var j = argc.length - 1 - (f.type == "multifunc" ? 1 : 0); j >= 0; j--){
					header += '	__let "' + argc[j] + ' __get 1\n';
				}
				if(f.type == "multifunc"){
					header += '] [\n';
					header += '	__let "' + argc[argc.length-1] + ' []\n';
					header += '	repeat :__system_multi [__let "' + argc[argc.length-1]
						+ ' __concat __eval "arrays_raml __get 2 :' + argc[argc.length-1] + ' ]\n';
					header += ']\n';
				}
				header += '__echolog "true\n';
				// ------------------------------------------- Конец
				if(noheader){
					header = "";
				}
				f.code = header + code;
				if(!noheader){
					f.code += "\n__end\n__checkret";
				}
				f.code = "[\n" + f.code + "\n]";
			}
			funcs.push(f);
		}
		return funcs;
	}
	LogoParse.file2funcs = function(file, cb){
		importText(file, function(txt){
			cb(LogoParse.lib2funcs(txt));
		});
	}
	callback();
}
// ------------------------------------------------------------------------------------------------------------
importFile("/codelib.js", Context, function(){
importFile("/textparse.js", Context, function(){
	CodeLib = Context.CodeLib;
	TextParse = Context.TextParse;
	loader(function(){
		Context.LogoParse = LogoParse;
		callback();
	});
})});