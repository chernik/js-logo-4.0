﻿var Context = this;

function getTime(){
	return (new Date()).getTime() / 1000;
}

var get = Context.format.get;
var put = Context.format.put;
var norma = Context.format.norma;

var nowTime = getTime();

Context.data = [
	"timer_get": function(args){
		return put(100 * (getTime() - nowTime));
	},
	"timer_reset": function(args){
		nowTime = getTime();
	}
];

callback();
