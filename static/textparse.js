﻿var TextParse = {};

TextParse.reg = function(str, reg, func, pind, arr){
	if(str === undefined){
		throw "TextParse > I need string!";
	}
	if(reg === undefined){
		throw "TextParse > I neer regular expr!";
	}
	if(pind === undefined){
		pind = [0];
	}
	if(arr === undefined){
		arr = [];
	}
	if(func === undefined){
		func = [];
	}
	var ind = pind[0];
	var coms = reg.split(" ");
	var pos = -1;
	var buf = "";
	while(pos >= -1){
		pos++;
		if(pos >= coms.length){
			throw "TextParse > Unexpected and at " + pos;
		}
		if(coms[pos] == ""){
			throw "TextParse > Wrong string at " + pos;
		}
		if(coms[pos][0] == ">"){
			var num = +coms[pos].substring(1);
			if(num != num){
				throw "TextParse > Wrong ind of func at " + pos;
			}
			var err;
			pind[0] = ind;
			try{
				func[num](str, pind, buf, arr);
			} catch(err){
				throw "TextParse > Func error > " + err + " at " + pos;
			}
			ind = pind[0];
			continue;
		}
		if(coms[pos] == "sp"){
			coms[pos] = "' ";
		}
		if(coms[pos][0] == "'"){
			var ch = coms[pos].substring(1);
			if(ch.length != 1){
				throw "TextParse > Wrong symbol at " + pos;
			}
			pos++;
			if(ind < str.length && str[ind] == ch){
				continue;
			}
			var num = +coms[pos];
			if(num != num){
				throw "TextParse > Wrong jump cond at " + pos;
			}
			pos += num;
			continue;
		}
		if(coms[pos] == "end"){
			pos++;
			if(ind >= str.length){
				continue;
			}
			var num = +coms[pos];
			if(num != num){
				throw "TextParse > Wrong jump cond at " + pos;
			}
			pos += num;
			continue;
		}
		switch(coms[pos]){
		case "m":
			ind++;
			break;
		case "p":
			if(ind >= str.length){
				throw "TextParse > Char doesn't exist at " + pos;
			}
			buf += str[ind];
			ind++;
			break;
		case "g":
			pos++;
			var num = +coms[pos];
			if(num != num){
				throw "TextParse > Wrong jump value at " + pos;
			}
			pos = num - 1;
			break;
		case "c":
			buf = "";
			break;
		case "stop":
			pos = -2;
			break;
		default:
			throw "TextParse > Wrong command at " + pos;
			break;
		}
	}
	pind[0] = ind;
	return arr;
}

TextParse.normalizeSpaces = function(str){
	str = " " + str + " ";
	str = str.split("\n").join(" ");
	str = str.split("	").join(" ");
	var coms;
	while(true){
		coms = str.split("  ");
		if(coms.length == 1){
			break;
		}
		str = coms.join(" ");
	}
	return str.substring(1, str.length-1);
}

TextParse.removeComments = function(str){
	str = this.normalizeSpaces(str);
	var coms = str.split(" ");
	var coms2 = [];
	for(var i = 0; i < coms.length; i++){
		if(coms[i][0] != ":"){
			coms2.push(coms[i]);
		}
	}
	return coms2.join(" ");
}

TextParse.tests = function(){
	// ---------------------------------------------
	var str = "abc";
	var reg = "p p p >0 stop";
	var fs = [function(str, ind, buf, arr){
		arr.push(buf);
	}];
	var res = this.reg(str, reg, fs);
	if(!(res.length == 1 && res[0] == "abc")){
		throw "TextParse > No test 1";
	}
	// --------------------------------------------
	str = '[ abc "def ghi |jkl mno| "| pqr | [1 2 3]]';
	reg = ">0 m sp 2 g 1 '\" 34 p '| 10 m '| 4 >1 c g 1 p g 12 sp 4 >1 c g 1 '[ 4 >1 c g 46 '] 4 >1 c g 51 p g 21 '| 2 g 11 '[ 3 >2 g 1 '] 1 stop g 27";
	fs = [
		function(str, pind, buf, arr){
			arr.push([]);
		},
		function(str, pind, buf, arr){
			arr[arr.length - 1].push(buf);
		},
		function(_str, pind, buf, arr){
			TextParse.reg(str, reg, fs, pind, arr);
			var res = arr.pop();
			arr[arr.length - 1].push(res);
		}
	];
	res = this.reg(str, reg, fs)[0];
	if(!(
		res.length == 6 &&
		res[0] == "abc" &&
		res[1] == "\"def" &&
		res[2] == "ghi" &&
		res[3] == "jkl mno" &&
		res[4] == "\" pqr " &&
		res[5].length == 3 &&
		typeof(res[5]) == "object" &&
		res[5][0] == "1" &&
		res[5][1] == "2" &&
		res[5][2] == "3")){
		throw "TextParse > No test 2";
	}
	// --------------------------------------------
	console.info('Fine!');
}

this.TextParse = TextParse;
callback();